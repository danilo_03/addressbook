import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class UsersService {

  constructor() {
    if (localStorage.getItem("Users") === null) {
      let Users = [];
      localStorage.setItem("Users", JSON.stringify(Users));
    }
  }

  getUsers () {
    return JSON.parse(localStorage.getItem('Users'));
  }


  createUser(user) {
    let users = this.getUsers();
    users.push(user);
    localStorage.setItem('Users', JSON.stringify(users));
    return {
      "message": "Usuario creado."
    }
  }

  getUserByUUID(uuid) {
    let users = JSON.parse(localStorage.getItem('Users'));
    return users.find(u => u.uuid === uuid);
  }

  deleteUserByUUID(uuid) {
    let users = this.getUsers();
    for (let i = 0; i<users.length; i++){
      if (users[i].uuid === uuid) {
        users.splice(i, 1);
      }
    }
    localStorage.setItem('Users', JSON.stringify(users));
    return {
      "message": "El usuario se ha eliminado."
    }
  }

  updateUserByUUID(uuid, update) {
    let users = this.getUsers();
    for (let i = 0; i<users.length; i++){
      if (users[i].uuid === uuid) {
        users[i] = update;
      }
    }
    localStorage.setItem('Users', JSON.stringify(users));
    return {
      "message": "Usuario actualizado."
    }
  }


  generateUUID() {
    let dt = new Date().getTime();
    let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
        let r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
  }
}
