import { Component, OnInit } from '@angular/core';

import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {


  users: any = [];
  previousPage: number;
  page: number = 1;

  constructor(private usersService: UsersService) {
  }

  ngOnInit(): void  {
    this.users = this.usersService.getUsers();
  }


  deleteUser(user) {
    this.usersService.deleteUserByUUID(user.uuid);
    this.users = this.usersService.getUsers();
  }


}
