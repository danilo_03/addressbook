import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(arr: any[], value: string): any {

    if (value !== undefined && value !== "") {

        let results = arr.filter(user => {
            let name = user.firstname+ " "+user.lastname;
            if (name.toLocaleLowerCase().indexOf(value.toLocaleLowerCase()) !== -1) {
                return user;
            }
        });

        return results;
    } else {
      return arr;
    }
  }
}