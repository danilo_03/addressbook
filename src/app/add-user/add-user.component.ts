import { Component, OnInit } from '@angular/core';
import { UsersService } from '../services/users.service';
import { Router } from "@angular/router";
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  constructor(
    private usersService: UsersService,
    private router: Router,
    private fb: FormBuilder) { }

  user: any = {};
  addUserForm: FormGroup;
  ngOnInit() {
    this.createForm();
  }


  createForm() {
    this.addUserForm = this.fb.group({
       firstname: ['', Validators.required ],
       lastname: ['', Validators.required],
       phone: ['', Validators.required],
       email: ['', [Validators.required , Validators.email]],
       birthdate: ['', Validators.required]
    });
  }

  addUser() {
    this.user = this.addUserForm.value;
    this.user.uuid = this.usersService.generateUUID();
    this.usersService.createUser(this.user);
    this.router.navigateByUrl("/");
  }


}
