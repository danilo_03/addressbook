import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../services/users.service';
import { FormGroup, FormControl,  FormBuilder,  Validators } from '@angular/forms';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usersService: UsersService,
    private fb: FormBuilder
  ) {}

  user: any = {};
  userUUID: string = "";
  editUserForm: FormGroup;


  ngOnInit() {
    this.userUUID = this.route.params['value'].uuid;
    this.user = this.usersService.getUserByUUID(this.userUUID);
    this.createForm();
  }

  createForm() {
    this.editUserForm = this.fb.group({
       firstname: [this.user.firstname, Validators.required ],
       lastname: [this.user.lastname, Validators.required],
       phone: [this.user.phone, Validators.required],
       email: [this.user.email, [Validators.required , Validators.email]],
       birthdate: [this.user.birthdate, Validators.required]
    });
  }


  updateUser() {
    this.user = this.editUserForm.value;
    this.user.uuid = this.userUUID;
    this.usersService.updateUserByUUID(this.userUUID, this.user);
    this.router.navigateByUrl("/");
  }

}
